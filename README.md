# README #

Simple library to facilitate tcp/ip socket connections.
When a client is created, it can connect multiple times, in order to allow multiple parallel connections, or to reconnect to a server.